function randomImg(){
    // set array backgrounds
    let arrayImg =['./img/0.jpg','./img/1.jpg','./img/2.jpg'];
    
    // random var
    let random = arrayImg[Math.floor(Math.random()*arrayImg.length)];

    // get img id
    let img = document.getElementById("back-img");

    // set the src property to random
    img.src = random;
}

document.addEventListener('load', randomImg());