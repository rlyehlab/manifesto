## Manifiesto rlab

Gitlab Page que únicamente sirve el manifiesto en una landing independiente.

![vista previa del manifiesto en version desktop](./public/img/snapshot.png)

Enlace gitlab page: [https://manifiesto.rlab.be](https://manifiesto.rlab.be)

Las imágenes en el back son 3, utilizando los distintos estilos que se plantearon para la identidad. Se muestra un motivo random cada vez que entramos a la landing page.

Diseños disponibles

<img src="./public/img/kawaii-view.jpg" width="300" alt="arte manifiesto version kawaii">
<img src="./public/img/dark-view.jpg" width="300" alt="arte manifiesto version dark">
<img src="./public/img/glitch-view.jpg" width="300" alt="arte manifiesto version glitch">

Este repo contiene [una versión markdown del Manifiesto](manifiesto.md)
